# Topics of Day-9 hands-on session

Parallel execution on CPUs and GPUs. Covered topics are:

* basic description of GPU acceleration,
* optimise CPU only runs,
* efficiently run on accelerated systems.

------------------------------------------------------------------------

**Nota bene:** this set of exercises will be performed on Marconi100, the HPC system installed at CINECA.

Please login on the cluster with the credentials you have been provided and collect the exercises with:

~~~~~bash
git clone \
https://gitlab.com/QEF/materials-for-max-qe2021-online-school.git
~~~~~



---

**Exercise 0:** Basic information about GPU acceleration.

    cd example0.intro/

**Exercise 1:** Setting up QE on CPU and GPU systems.

    cd example1.setup/

**Exercise 2:** Parallel options -- improve performance with npool and ndiag

    cd example2.CPU/

**Exercise 3:** Accelerated systems -- how to run with NVidia GPUs

    cd example3.GPU/
